package xyz.daos.annotation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import xyz.daos.annotation.service.Spammer;

@SpringBootApplication
public class Application {
    public static void main(final String[] args) throws InterruptedException {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        Spammer spammer = context.getBean(Spammer.class);
        spammer.spam(10, 1);
        System.out.println(".........................................................................................");
        spammer.tryBlockSpam(10, 1);
    }
}
