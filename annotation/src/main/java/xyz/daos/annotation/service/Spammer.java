package xyz.daos.annotation.service;

import org.springframework.stereotype.Component;
import xyz.daos.annotation.aspect.LogMe;
import xyz.daos.annotation.aspect.SkipMe;

import java.util.concurrent.TimeUnit;

@Component
public class Spammer {
    @LogMe
    public void spam(int repetitions, int delay) throws InterruptedException {
        for (int i = 0; i < repetitions; i++) {
            System.out.println(System.currentTimeMillis());
            TimeUnit.MILLISECONDS.sleep(delay);
        }
    }

    @SkipMe
    public void tryBlockSpam(int repetitions, int delay) throws InterruptedException {
        spam(repetitions, delay);
    }
}
