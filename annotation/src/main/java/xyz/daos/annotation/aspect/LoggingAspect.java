package xyz.daos.annotation.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
public class LoggingAspect {

    @Pointcut("@annotation(LogMe)")
    public void loggingPointcut(){}

    @Pointcut("@annotation(SkipMe)")
    public void skipPointcut(){}

    @Before("loggingPointcut()")
    public void logBefore(JoinPoint joinPoint){
        log.info("=======LOG BEFORE=======");
        log.info("Method arguments: ");
        log.info("repetitions - " + joinPoint.getArgs()[0]);
        log.info("delay - " + joinPoint.getArgs()[1] + " ms");
        log.info("========================");
    }

    @AfterReturning("@annotation(LogMe)")
    public void logAfter(JoinPoint joinPoint) {
        log.info("=======LOG AFTER=======");
        log.info(joinPoint.getSignature().getName() + " executed successfully");
        log.info("========================");
    }

    @Around("@annotation(SkipMe)")
    public void logSkip(JoinPoint joinPoint) {
        log.info("=======METHOD EXECUTION SKIPPED=======");
    }

}
