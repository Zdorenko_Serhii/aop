package xyz.daos.xml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ImportResource;
import xyz.daos.xml.service.Spammer;

@SpringBootApplication
@ImportResource("applicationContext.xml")
public class Application {
    public static void main(final String[] args) throws InterruptedException {
        ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        context.getBean(Spammer.class).spam(10, 50);
    }
}
