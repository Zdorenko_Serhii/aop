package xyz.daos.xml.aspect;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExceptionAspect {
    public void logException(Exception ex){
        log.info("==========Log Exception " + ex.getMessage() + "==========");
    }
}
