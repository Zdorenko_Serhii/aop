package xyz.daos.xml.aspect;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AfterAspect {
    public void logAfter(){
        log.info("==========Log after method execution (xml config)==========");
    }
}
