package xyz.daos.xml.aspect;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BeforeAspect {
    public void logBefore() {
        log.info("==========Log before method execution (xml config)==========");
    }
}
