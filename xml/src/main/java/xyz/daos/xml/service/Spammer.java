package xyz.daos.xml.service;

import java.util.concurrent.TimeUnit;

public class Spammer {
    public void spam(int repetitions, int delay) throws InterruptedException {
        for (int i = 0; i < repetitions; i++) {
            if (System.currentTimeMillis() % 33 == 0) throw new InterruptedException("Message to log");
            System.out.println(System.currentTimeMillis());
            TimeUnit.MILLISECONDS.sleep(delay);
        }
    }
}
